# Reaktor Pre-Assignment Summer Job 2020

Program written in Node.js that displays information about packages from the /var/lib/dpkg/status file via an HTML interface.

Available at [reaktor.cedricdose.com](https://reaktor.cedricdose.com)

## About

This program is written with **no** external dependencies, just Node.js, HTML & CSS.

All pages are rendered on the server.

## Usage

    node index.js



