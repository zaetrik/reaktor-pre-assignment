FROM node:10.17.0-alpine

RUN addgroup -S user && adduser -S -g user user

ENV HOME=/home/user

COPY package.json package-lock.json $HOME/app/

COPY / $HOME/app/

WORKDIR $HOME/app

RUN chown -R user:user $HOME/* /usr/local/ && \
    npm install --silent --progress=false && \
    chown -R user:user $HOME/*

USER user

EXPOSE 8080

CMD node index.js
