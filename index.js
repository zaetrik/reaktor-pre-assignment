const startServer = require("./server/server");
const getPackageData = require("./server/services/data/dataProcessor");

getPackageData().then(startServer);
