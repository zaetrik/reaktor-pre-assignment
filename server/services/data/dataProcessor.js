const fs = require("fs");
const dataStore = require("./dataStore");

/**
 * Reads status file & stores all packages in store
 * @returns { Promise<any> }
 */
module.exports = async () => {
  const statusFile = await readStatusFile();

  statusFile
    .split("\n\n")
    .filter(item => item.match(/^Package:/))
    .map(package => packageToStore(generatePackageObject(package + "END")));
};

/**
 * @returns { Promise<string> } status file
 */
const readStatusFile = async () => {
  return new Promise((resolve, reject) => {
    const pathRealData = "/var/lib/dpkg/status";
    const pathMockData = "./server/data/mockData";
    const actualPath = fs.existsSync(pathRealData)
      ? pathRealData
      : pathMockData;

    fs.readFile(actualPath, "utf8", (err, data) => {
      if (err) reject(err);
      else resolve(data);
    });
  });
};

/**
 * Generates {package: string, depends: string, description: string} from a package string
 * @param { string } packageString
 * @returns { {package: string, depends: string, description: string} }
 */
const generatePackageObject = packageString => {
  return {
    description: transformDescription(
      getValueForKeyFromString("Description: ", packageString)
    ),
    package: getValueForKeyFromString("Package: ", packageString),
    depends: getValueForKeyFromString("Depends: ", packageString)
  };
};

/**
 * @param { string } key
 * @param { string } string
 */
const getValueForKeyFromString = (key, string) => {
  const regex = new RegExp(`(?<=${key})[\\s\\S]+?(?=\\n[A-Z]|END)`);
  const value = string.match(regex);
  return value ? value[0] : undefined;
};

/**
 * Transform description string
 * @param { string } descriptionString
 * @returns { string }
 */
const transformDescription = descriptionString => {
  if (!descriptionString) return undefined;

  return descriptionString
    .replace("\n", "<br/><br/>")
    .split("\n")
    .map(line => {
      if (line.trim() === ".") return "<br/>";
      else return line.trim() + "<br/>";
    })
    .join("");
};

/**
 * Adds package to store
 * @param { {package: string, depends: string, description: string} } package
 */
const packageToStore = package => dataStore.add(package.package, package);
