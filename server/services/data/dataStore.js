/**
 * Store for packages
 */
module.exports = {
  data: {},
  add: function(key, value) {
    this.data[key] = value;
  },
  get: function(key) {
    return this.data[key];
  },
  getAll: function() {
    return this.data;
  },
  getReverseDependencies: function(packageName) {
    return Object.keys(this.data)
      .filter(package => this.data[package].depends)
      .map(package => {
        const regexMatchPackageName = new RegExp(
          "\\b" + packageName.replace(/[.*+?^${}()|[\]\\]/g, "\\$&") + "\\b"
        );

        if (this.data[package].depends.match(regexMatchPackageName))
          return package;
      })
      .filter(package => package);
  }
};
