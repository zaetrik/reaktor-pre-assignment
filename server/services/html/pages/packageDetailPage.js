const dataStore = require("../../data/dataStore");

/**
 * Generates HTML for package detail page
 * @param { {package: string, depends: string, description: string} } packageData
 * @return { string } HTML
 */
module.exports = packageData => {
  return `<div class="package-details-container">
            <h1 class="package-details-name">${packageData.package}</h1>
            <p class="package-details-item">
              <strong>Description:</strong>
              <br/>
              ${packageData.description}
            </p>
            ${renderDependencies(packageData)}
            ${renderReverseDependencies(packageData)}
          </div>`;
};

/**
 * @param { {package: string, depends: string, description: string} } packageData
 * @returns { string } HTML for dependencies
 */
const renderDependencies = packageData => {
  return packageData.depends
    ? `
      <p class="package-details-item">
        <strong>Dependencies: </strong>
        <ul>
          ${getDependenciesListItems(packageData)}
        </ul>
      </p>`
    : "";
};

/**
 * @param { {package: string, depends: string, description: string} } packageData
 * @returns { string }
 */
const getDependenciesListItems = packageData => {
  return packageData.depends
    .split(",")
    .map(dependency => {
      if (dependency.indexOf("|") !== -1) {
        return renderDependsAlternatives(dependency.split("|"));
      } else {
        return getListItem(dependency);
      }
    })
    .join("");
};

/**
 * @param { string[] } dependencies
 * @returns { string } HTML for alternative dependencies
 */
const renderDependsAlternatives = dependencies => {
  return dependencies
    .map(dependency => {
      return getListItem(dependency);
    })
    .join("");
};

/**
 * @param { {package: string, depends: string, description: string} } packageData
 * @returns { string } HTML for reverse dependencies
 */
const renderReverseDependencies = packageData => {
  const reverseDependencies = dataStore.getReverseDependencies(
    packageData.package
  );

  return reverseDependencies.length > 0
    ? `
      <p class="package-details-item">
        <strong>Reverse Dependencies: </strong>
        <ul>
          ${reverseDependencies
            .map(reverseDependency => getListItem(reverseDependency))
            .join("")}
        </ul>
      </p>`
    : "";
};

/**
 * Generates HTML string for list items
 * @param { string } dependency
 * @returns { string } HTML list item
 */
const getListItem = dependency => {
  return `
      <li class="package-details-list-item">
        ${
          dataStore.get(cleanDependency(dependency))
            ? `
              <a href="/packages/${cleanDependency(dependency)}">
                ${cleanDependency(dependency)}
              </a>`
            : cleanDependency(dependency)
        }
      </li>`;
};

/**
 * Cleans dependency string; removes version number
 * @param { string } dependency
 * @returns { string }
 */
const cleanDependency = dependency => {
  return dependency.trim().split(" ")[0];
};
