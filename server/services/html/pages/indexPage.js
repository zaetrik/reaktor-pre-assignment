const dataStore = require("../../data/dataStore");

module.exports = () => {
  return `
  <div class="packages-list-container">
    <ul id="packages-list">
        ${getAllPackagesList()}  
    </ul>
  </div>`;
};

/**
 * @returns { string } HTML <li> element
 */
const getAllPackagesList = () => {
  return Object.keys(dataStore.getAll())
    .sort()
    .map(package => {
      return `<li class="packages-list-item"><a href="/packages/${package}">${package}</a></li>`;
    })
    .join("");
};
