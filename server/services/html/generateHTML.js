const indexPage = require("./pages/indexPage");
const packageDetailPage = require("./pages/packageDetailPage");

module.exports = {
  indexPage: function() {
    return getHTML(indexPage());
  },
  packageDetailPage: function(packageData) {
    return getHTML(packageDetailPage(packageData));
  }
};

/**
 * Generates HTML
 * @param string customHTML to be inserted into page
 * @returns string HTML
 */
const getHTML = customHTML => {
  return `
      <html lang="en">
        <head>
          <title>Reaktor Pre-Assignment</title>
          <meta
            name="viewport"
            content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/static/assets/favicon.png"
          />
          <link href="/static/css/styles.css" rel="stylesheet" />
          <link
            href="https://fonts.googleapis.com/css?family=Montserrat&display=swap"
            rel="stylesheet"
          />
        </head>
        <body>
          <nav>
            <div class="nav-item">
              <a href="/">All Packages</a>
            </div>
          </nav>
          <main>
            ${customHTML}
          </main>
        </body>
      </html>`;
};
