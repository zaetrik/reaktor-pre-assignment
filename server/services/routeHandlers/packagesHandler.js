const dataStore = require("../data/dataStore");
const generateHTML = require("../html/generateHTML");
const errorHandler = require("./utils/errorHandler");
const sendResponse = require("./utils/sendResponse");
const url = require("url");

module.exports = (request, response) => {
  const requestHandler = routes[request.method];

  if (requestHandler) requestHandler(request, response);
  else errorHandler(response);
};

const routes = {
  GET: async (request, response) => {
    const requestUrl = url.parse(request.url, true);

    if (requestUrl.path === "/") {
      routeHandlers.handleGetIndexPage(response);
    } else if (
      requestUrl.path.split("/")[1] === "packages" &&
      requestUrl.path.split("/").length === 3
    ) {
      routeHandlers.handleGetPackage(request, response);
    } else {
      errorHandler(response);
    }
  }
};

const routeHandlers = {
  handleGetIndexPage: function(response) {
    const responseConfig = {
      "Content-Type": "text/html"
    };
    const html = generateHTML.indexPage();
    sendResponse(response, html, responseConfig);
  },
  handleGetPackage: function(request, response) {
    const requestUrl = url.parse(request.url, true);
    const packageName = requestUrl.path.split("/")[2];

    const responseConfig = {
      "Content-Type": "text/html"
    };

    const packageData = dataStore.get(packageName);
    if (!packageData) return errorHandler(response);

    const html = generateHTML.packageDetailPage(packageData);
    sendResponse(response, html, responseConfig);
  }
};
