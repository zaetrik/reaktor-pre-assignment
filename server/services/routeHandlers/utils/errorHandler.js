/**
 * Send 404 error response
 */
module.exports = response => {
  response.writeHead(404);
  response.write("NOT FOUND");
  response.end();
};
