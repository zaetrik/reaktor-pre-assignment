module.exports = (response, data, config) => {
  response.writeHead(200, config);
  response.end(data);
};
