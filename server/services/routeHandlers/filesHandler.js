const errorHandler = require("./utils/errorHandler");
const sendResponse = require("./utils/sendResponse");
const fs = require("fs");
const url = require("url");

/**
 * Handler for files
 */
module.exports = (request, response) => {
  const requestHandler = routes[request.method];

  if (requestHandler) requestHandler(request, response);
  else errorHandler(response);
};

const routes = {
  GET: async (request, response) => {
    const requestUrl = url.parse(request.url, true);

    if (requestUrl.path === "/static/css/styles.css") {
      await routeHandlers.handleStylesRequest(response);
    } else if (requestUrl.path === "/static/js/index.js") {
      await routeHandlers.handleIndexJavascriptRequest(response);
    } else if (requestUrl.path === "/static/assets/favicon.png") {
      await routeHandlers.handleFaviconRequest(response);
    } else {
      errorHandler(response);
    }
  }
};

const routeHandlers = {
  handleStylesRequest: async function(response) {
    const responseConfig = {
      "Content-Type": "text/css"
    };
    const css = await readFile(response, "./client/css/styles.css");
    sendResponse(response, css, responseConfig);
  },
  handleIndexJavascriptRequest: async function(response) {
    const responseConfig = {
      "Content-Type": "text/javascript"
    };
    const javascript = await readFile(response, "./client/js/index.js");
    sendResponse(response, javascript, responseConfig);
  },
  handleFaviconRequest: async function(response) {
    const responseConfig = {
      "Content-Type": "image/png"
    };
    const favicon = await readFile(response, "./client/assets/favicon.png");
    sendResponse(response, favicon, responseConfig);
  }
};

/**
 * @param { response, path: string }
 * @param { string } path
 */
const readFile = (response, path) => {
  return new Promise(resolve => {
    fs.readFile(path, null, function(error, data) {
      if (error) errorHandler(response);
      else resolve(data);
    });
  });
};
