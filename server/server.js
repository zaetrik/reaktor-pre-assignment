const http = require("http");
const packagesHandler = require("./services/routeHandlers/packagesHandler");
const filesHandler = require("./services/routeHandlers/filesHandler");
const errorHandler = require("./services/routeHandlers/utils/errorHandler");

/**
 * Registers routes & starts server
 */
module.exports = () => {
  const routes = {
    "/": packagesHandler,
    "/packages": packagesHandler,
    "/static": filesHandler
  };

  const server = http.createServer((request, response) => {
    const route = routes[`/${request.url.split("/")[1]}`];

    if (route) route(request, response);
    else errorHandler(response);
  });

  server.listen(8080);
};
